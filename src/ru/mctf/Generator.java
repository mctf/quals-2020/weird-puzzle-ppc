package ru.mctf;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Generator {

    private final Box box = new Box();

    public static final String FLAG = "mctf{alg0s_kru1o}";

    public Box generate() {
        assert FLAG.chars().distinct().count() == FLAG.length();

        Field firstField = box.getField(0);

        for (int i = 0; i < FLAG.length(); i++) {
            firstField.setFromAlphabet(i, FLAG.charAt(i));
        }

        step(FLAG.length());
        return box;
    }

    private boolean step(int num) {
        int fieldNum = num / Field.SIZE;
        int elementNum = num % Field.SIZE;
        Field field = box.getField(fieldNum);

        List<Integer> numbers = IntStream.iterate(1, i -> i <= Field.SIZE, i -> i + 1)
                .boxed()
                .collect(Collectors.toList());

        numbers.removeAll(field.getUsedNumbers());
        numbers.removeAll(box.getUsedNumbersInLine(elementNum));

        Collections.shuffle(numbers);

        for (Integer number : numbers) {
            field.set(elementNum, number);

            if (num == Box.SIZE - 1) {
                return true;
            } else {
                boolean success = step(num + 1);
                if (success) {
                    return true;
                }

                for (int i = num + 1; i < Box.SIZE; i++) {
                    cleanElement(i);
                }
            }
        }

        return false;
    }

    private void cleanElement(int num) {
        int fieldNum = num / Field.SIZE;
        int elementNum = num % Field.SIZE;
        Field field = box.getField(fieldNum);
        field.set(elementNum, Field.UNSET);
    }

}
