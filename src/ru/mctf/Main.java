package ru.mctf;

import java.io.FileWriter;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Box box = new Generator().generate();

        FileWriter fileWriter = new FileWriter("filled.txt");
        box.print(fileWriter);
        fileWriter.close();

        System.out.println("Generated");

        Box reduced = new Reducer(box.copy()).reduce();
        fileWriter = new FileWriter("reduced.txt");
        reduced.print(fileWriter);
        fileWriter.close();
        reduced.print();
        System.out.println("Reduced");

        long start = System.currentTimeMillis() / 1000;
        Box solution = new Solver(reduced).solve();
        long end = System.currentTimeMillis() / 1000;
        System.out.println("Solved in " + (end - start) + " seconds");
        assert solution.validate();
        assert solution.equals(box);

        fileWriter = new FileWriter("solved.txt");
        solution.print(fileWriter);
        fileWriter.close();

        System.out.println("Solved");
        solution.print();
    }



}
