package ru.mctf;

import java.util.Collections;
import java.util.LinkedList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Reducer {

    private final Box box;
    private final LinkedList<Integer> nums;

    private Box bestBox;
    private int bestReduce = 0;

    private static final int COMPLEXITY = 260;

    public Reducer(Box box) {
        this.box = box;

        nums = IntStream.iterate(Generator.FLAG.length(), i -> i < Box.SIZE, i -> i + 1)
                .boxed()
                .collect(Collectors.toCollection(LinkedList::new));
        Collections.shuffle(nums);

        IntStream.iterate(5, i -> i < Generator.FLAG.length(), i -> i + 1)
                .forEach(nums::addFirst);
    }

    public Box reduce() {
        step(1);
        return bestBox;
    }

    private void step(int stepNum) {
        if (nums.isEmpty()) {
            return;
        }

        Integer num = nums.pop();

        int fieldNum = num / Field.SIZE;
        int elementNum = num % Field.SIZE;

        Field field = box.getField(fieldNum);
        int oldVal = field.get(elementNum);
        field.set(elementNum, Field.UNSET);

        if (trySolve()) {
            if (stepNum > bestReduce) {
                bestReduce = stepNum;
                bestBox = box.copy();
                System.out.println("Reduced by " + bestReduce);

                if (bestReduce == COMPLEXITY) {
                    return;
                }
            }

            step(stepNum + 1);
        } else {
            field.set(elementNum, oldVal);
            step(stepNum);
        }
    }

    private boolean trySolve() {
        try {
            Box solved = new Solver(box).solve();
            return solved != null;
        } catch (IllegalStateException e) {
            return false;
        }
    }

}
