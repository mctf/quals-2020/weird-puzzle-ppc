package ru.mctf;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solver {

    private final Box initialBox;
    private final Box overlayBox = new Box();

    private Box solution = null;

    public Solver(Box initialBox) {
        this.initialBox = initialBox;
    }

    public Box solve() {
        step(0);
        return solution;
    }

    private void step(int num) {
        int fieldNum = num / Field.SIZE;
        int elementNum = num % Field.SIZE;

        if (initialBox.getField(fieldNum).get(elementNum) != Field.UNSET) {
            if (num == Box.SIZE - 1) {
                if (solution != null) {
                    throw new IllegalStateException("Multiple solutions exist");
                }

                solution = initialBox.merge(overlayBox);
            } else {
                step(num + 1);
                return;
            }
        }

        Field field = overlayBox.getField(fieldNum);

        List<Integer> numbers = IntStream.iterate(1, i -> i <= Field.SIZE, i -> i + 1)
                .boxed()
                .collect(Collectors.toList());

        numbers.removeAll(getUsedNumbers(fieldNum));
        numbers.removeAll(getUsedNumbersInLine(elementNum));

        for (Integer number : numbers) {
            field.set(elementNum, number);

            if (num == Box.SIZE - 1) {
                if (solution != null) {
                    throw new IllegalStateException("Multiple solutions exist");
                }

                solution = initialBox.merge(overlayBox);
            } else {
                step(num + 1);

                for (int i = num + 1; i < Box.SIZE; i++) {
                    cleanElement(i);
                }
            }
        }
    }

    private Set<Integer> getUsedNumbers(int fieldNum) {
        Set<Integer> used = new HashSet<>();

        Field initialField = initialBox.getField(fieldNum);
        Field overlayField = overlayBox.getField(fieldNum);

        used.addAll(initialField.getUsedNumbers());
        used.addAll(overlayField.getUsedNumbers());

        return used;
    }

    private Set<Integer> getUsedNumbersInLine(int line) {
        Set<Integer> used = new HashSet<>();

        used.addAll(initialBox.getUsedNumbersInLine(line));
        used.addAll(overlayBox.getUsedNumbersInLine(line));

        return used;
    }

    private void cleanElement(int num) {
        int fieldNum = num / Field.SIZE;
        int elementNum = num % Field.SIZE;
        Field field = overlayBox.getField(fieldNum);
        field.set(elementNum, Field.UNSET);
    }

}
